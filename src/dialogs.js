/**
 *  Create dialog based on JQuery Dialogs plugin
 *
 *  All the below config properties are optional except for
 *  the htmlId property. See below html element
 *
 * config = {
 *      htmlId: "dialog-1",
 *      title: "This is Dialog 1",
 *      resizable: true,
 *      height: "auto",
 *      width: 400,
 *      modal: true,
 *      okButtonText: "Ok",
 *      cancelButtonText: "Cancel",
 *      callback: function (selection) {
 *         // callback when button okButton, cancelButton for "x" is selected.
 *         // The  text of the button is returned in selection.
 *         // For the "x" button,  "x" is returned.
 *      }
 *      openInit: function (evt,ui {
 *          // call when the dialog is open, any content
 *          // in the dialog for example, a selection list
 *          // or text field can be set.
 *      }
 * }
 *
 * A simple html element might look something like:
 *
 *  <div id="dialog-1"  >
 *  <p>
 *      These items will be permanently deleted and cannot be recovered. Are you sure?
 *  </p>
 *  </div>
 *
 * A simple dialog with all the config "defaults" using above html element might look something like:
 *
 * let myDialog = createDialog({
 *       htmlId: "dialog-1"
 *    });
 *
 * Then to launch the dialog:
 *
 *  myDialog.dialog("open");
 *
 *
 * @param config
 * @returns {jQuery dialog}
 */


function createDialog(config) {
    let dialog = null;
    let configObj = {};

    configObj.autoOpen = false;
    configObj.resizable = config.resizable || false;
    configObj.height = config.height || "auto";
    configObj.width = config.width || "auto";
    configObj.modal = (!config.modal === undefined || config.modal !== false) ? true : false;
    configObj.title = config.title || "Dialog";
    configObj.close = function (evt) {
        // this always gets called, need to distinguish between the
        // "x" button or the close/confirm  button
        if (evt.originalEvent && config.callback) {
            config.callback("x");
        }
    };

    configObj.buttons = [];

    if (config.cancelButtonText) {
        configObj.buttons.push({
            text: config.cancelButtonText,
            open: function () {
                $(this).addClass('cancel-cls');
            },
            click: function () {
                $(this).dialog("close");
                if (config.callback) {
                    config.callback(config.cancelButtonText);
                }
            }
        });
    }

    if (config.okButtonText) {
        configObj.buttons.push({
            text: config.okButtonText,
            open: function () {
                $(this).addClass('ok-cls');
            },
            click: function () {
                $(this).dialog("close");
                if (config.callback) {
                    config.callback(config.okButtonText);
                }
            }
        });
    }

    if (config.openInit) {
        configObj.open = config.openInit;
    }

    // finally, create a new jquery dialog
    dialog = $("#" + config.htmlId).dialog(configObj);

    return dialog;
}
