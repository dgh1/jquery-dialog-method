

// modal popup references
let dialogOne = null;
let dialogTwo = null;
let dialogThree = null;
let dialogFour = null;
let dialogFive = null;


function onloadInit() {

    $(document).on('click', '#button-1', function (event) {
        event.preventDefault();
        buttonOne();
    });
    $(document).on('click', '#button-2', function (event) {
        event.preventDefault();
        buttonTwo();
    });
    $(document).on('click', '#button-3', function (event) {
        event.preventDefault();
        buttonThree();
    });
    $(document).on('click', '#button-4', function (event) {
        event.preventDefault();
        buttonFour();
    });
    $(document).on('click', '#button-5', function (event) {
        event.preventDefault();
        buttonFive();
    });


    dialogOne = createDialog({
        htmlId: "dialog-1",
        title: "This is Dialog 1",
        resizable: true,
        height: 100,
        width: 300,
        modal: true,
        okButtonText: "Ok",
        cancelButtonText: "Cancel",
        callback: function (selection) {
            displayText("Dialog 2  Button: " + selection);
        }

    });

    dialogTwo = createDialog({
        htmlId: "dialog-2",
        title: "Animal Selection",
        resizable: true,
        height: "auto",
        width: 225,
        modal: true,
        okButtonText: "Ok",
        callback: function (selection) {
            displayText("Dialog 2  Action: " + selection);
            if (selection === "Ok") {
                var e = document.getElementById("animal-selection");
                displayText ("You selected: " + e.options[e.selectedIndex].value);
            }
        },
        openInit: function (/*evt,ui*/) {
           document.getElementById("animal-selection").value = "";
        }
    });

    dialogThree = createDialog({
        htmlId: "dialog-3",
        title: "This is a non-modal dialog",
        resizable: true,
        height: 100,
        width: 410,
        modal: false,
        callback: function (selection) {
            displayText("Dialog 3  Button: " + selection);
        }
    });

    dialogFour = createDialog({
        htmlId: "dialog-4",
        title: "This is Dialog 4",
        resizable: true,
        height: "auto",
        width: 400,
        modal: true,
        okButtonText: "Got It",
        cancelButtonText: "Have no idea",
        callback: function (selection) {
            if (selection === "Have no idea") {
                displayText("Dialog 4  Button: " + selection + " - Answer: It was planet Ork");
            }
            else {
                let answer =  document.getElementById("answer-planet").value;
                if (answer.toLowerCase() === "ork") {
                    displayText("Dialog 4  Button: " + selection + " - Answer: That is correct");
                }
                else {
                    displayText("Dialog 4  Button: " + selection + " - Sorry!! that is incorrect");
                }
            }
        }

    });

    dialogFive = createDialog({
        htmlId: "dialog-5"
    });

}

function displayText (text) {
    document.getElementById("dialog-response").innerHTML = text;
}

function buttonOne() {
    dialogOne.dialog("open");
    displayText("");
}

function buttonTwo() {
    dialogTwo.dialog("open");
    displayText("");
}

function buttonThree() {
    dialogThree.dialog("open");
    displayText("");
}

function buttonFour() {
    dialogFour.dialog("open");
    displayText("");
}

function buttonFive() {
    dialogFive.dialog("open");
    displayText("");
}


