# Create dialog based on JQuery Dialogs plugin
  

## config input:

##### All the below config properties are optional except for the htmlId property.

```
   config = {
        htmlId: "dialog-1",
        title: "This is Dialog 1",
        resizable: true,
        height: "auto",
        width: 400,
        modal: true,
        okButtonText: "Ok",
        cancelButtonText: "Cancel",
        callback: function (selection) {
           // callback when button okButton, cancelButton for "x" is selected.
           // The  text of the button is returned in selection.
           // For the "x" button,  "x" is returned.
        }
        openInit: function (evt,ui {
            // callback when the dialog is opened, any content
            // in the dialog for example, a selection list
            // or text field can be initialized.
        }
   }
```

# A simple html element might look something like:
  
    <div id="dialog-1"  >
    <p>
        This dialog has all the default config pararmeters
    </p>
    </div>
  
#   A simple dialog using all the config "defaults"  with above html element might look something like:
```
   let myDialog = createDialog({
         htmlId: "dialog-1"
      });
```
#   Then to launch the dialog:
  
    myDialog.dialog("open");
  

![GitHub Logo](/docs/simple-dialog.png)

# Usage

    npm install jquery-dialog-method --dev-save

Since this uses the JQuery  plugins, css and images file you will need to include the following in your project

    npm install jquery --dev-save


Then add below to your index.html

The css files

```
  <link rel="stylesheet" href="node_modules/jquery-dialog-method/dist/css/jquery-ui.css">

  <!-- NOTE: when overriding jquery.ui classes make sure  jquery-ui.css is loaded first -->

   <link href="node_modules/jquery-dialog-method/dist/css/popup-dialogs.css" rel="stylesheet" type="text/css">
```

The  js files

```
 <script src="node_modules/jquery-dialog-method/dist/dialogs.js"></script>

 <script src="node_modules/jquery/dist/jquery.min.js"></script>

 <script src="node_modules/jquery-dialog-method/dist/jquery-ui.js"></script>

```

# Running the Demo

1. clone the project

* git clone https://gitlab.com/dgh1/jquery-dialog-method.git

2. cd to the project directory and 

* npm install
* npm run build



3. Enter the following in your browser:

* file:///\<project-root-dir\>/jquery-dialog-method/index.html


